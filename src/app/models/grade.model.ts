export class Grade {
    id: string;
    name: string;
    books: Book[];
}

export class Book {
    title: string;
    category: string;
    location: string;
}

export const CATEGORIES: string[] = [
    "Language",
    "Science",
    "Arts"
];