import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Grade, CATEGORIES, Book } from 'src/app/models/grade.model';
import { ActivatedRoute } from '@angular/router';
import { GradeService } from 'src/app/services/grade.service';

@Component({
  selector: 'app-grade-details',
  templateUrl: './grade-details.component.html',
  styleUrls: ['./grade-details.component.css']
})
export class GradeDetailsComponent implements OnInit {
  newBook: boolean = false;

  constructor(private route: ActivatedRoute,
    private gradeService: GradeService,
    private location: Location) { }

  grade: Grade;
  categories: string[];

  ngOnInit() {
    this.categories = CATEGORIES;
    this.getGrade();
  }

  getGrade(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.gradeService.getGrade(id)
      .subscribe(grade => this.grade = grade);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.gradeService.updateGrade(this.grade)
      .subscribe(() => this.goBack());
  }

  onFileChange(event) {
    console.log(event);
    // this.grade.books[0]=event.target.files;
  }

  addBook(): void {
    this.newBook = !this.newBook;
  }

  deleteBook(book: Book): void {
    let index = this.grade.books.indexOf(book);
    if (index > -1)
      this.grade.books.splice(index, 1);
  }

}
