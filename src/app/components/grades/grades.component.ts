import { Component, OnInit } from '@angular/core';
import { Grade } from 'src/app/models/grade.model';
import { GradeService } from 'src/app/services/grade.service';

@Component({
  selector: 'app-grade',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css']
})
export class GradesComponent implements OnInit {

  grades: Grade[];

  constructor(private gradeService: GradeService) { }

  ngOnInit() {
    this.getGrades();
  }

  getGrades() {
    this.gradeService.getGrades().subscribe(
      grades => {
        this.grades = grades;
      }
    );
  }

  addGrade(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.gradeService.addGrade({ name } as Grade)
      .subscribe(grade => {
        this.grades.push(grade);
      });
  }

  delete(grade: Grade): void {
    this.grades = this.grades.filter(g => g !== grade);
    this.gradeService.deleteGrade(grade).subscribe();
  }

}
