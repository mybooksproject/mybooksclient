import { Component, OnInit, Input } from '@angular/core';
import { Grade, Book, CATEGORIES } from 'src/app/models/grade.model';
import { GradeService } from 'src/app/services/grade.service';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.css']
})
export class NewBookComponent implements OnInit {

  @Input()
  grade: Grade;

  book: Book = { title: "", category: "", location: "" };
  categories: string[];
  selectedFile: FileList;

  constructor(private gradeService: GradeService) { }

  ngOnInit() {
    this.categories = CATEGORIES;
  }

  onFileChange(event: any) {
    this.selectedFile = event.target.files;
  }

  saveBook(): void {
    if (this.grade.books)
      this.grade.books.push(this.book);
    else
      this.grade.books = [this.book];
    if (this.selectedFile.item)
      this.gradeService.saveBook(this.grade, this.book, this.selectedFile.item(0))
        .subscribe(data => console.log("book saved!!!"));
    this.book = { title: "", category: "", location: "" };
  }

}
