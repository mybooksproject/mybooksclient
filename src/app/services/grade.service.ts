import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Grade, Book } from '../models/grade.model';
import { of, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GradeService {

  // private gradeUrl = 'api/grades'; // URL to web api
  private gradeUrl = 'server/grades'; // URL to server api

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getGrade(id: string): Observable<Grade> {
    const url = `${this.gradeUrl}/${id}`;
    return this.http.get<Grade>(url).pipe(
      tap(_ => this.log(`fetched grade id=${id}`)),
      catchError(this.handleError<Grade>(`getGrade id=${id}`))
    );
  }

  /** GET: get all grades from ther server */
  getGrades(): Observable<Grade[]> {
    console.log(this.gradeUrl);
    return this.http.get<any>(this.gradeUrl);
  }

  /** PUT: update a grade on the server */
  updateGrade(grade: Grade): Observable<any> {
    const id = typeof grade === 'string' ? grade : grade.id;
    const url = `${this.gradeUrl}/${id}`;

    return this.http.put(url, grade, this.httpOptions).pipe(
      tap(_ => this.log(`updated grade id=${grade.id} and name=${grade.name}`)),
      catchError(this.handleError<any>('updateGrade'))
    );
  }

  /** POST: add a grade to the server */
  addGrade(grade: Grade): Observable<Grade> {
    return this.http.post<Grade>(this.gradeUrl, grade, this.httpOptions).pipe(
      tap(_ => this.log(`added grade with id=${grade.id}`)),
      catchError(this.handleError<Grade>('addGrade'))
    );
  }

  /** POST: save a Book to the server */
  saveBook(grade: Grade, book: Book, file: File) {
    const url = this.gradeUrl + "/book";
    let formData: FormData = new FormData();
    formData.append('grade',JSON.stringify(grade));
    formData.append('book',JSON.stringify(book));
    formData.append('file',file);

    formData.forEach(data => console.log(data));

    return this.http.post<Grade>(url,formData).pipe(
      tap(_ => this.log(`saved book for grade with id=${grade.id}`)),
      catchError(this.handleError<Grade>('saveBook'))
    );
  }

  /** DELETE: delete the grade from the server */
  deleteGrade(grade: Grade | string): Observable<Grade> {
    const id = typeof grade === 'string' ? grade : grade.id;
    const url = `${this.gradeUrl}/${id}`;

    return this.http.delete<Grade>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted grade id=${id}`)),
      catchError(this.handleError<Grade>('deleteGrade'))
    );
  }

  /** Log a GradeService message with the MessageService */
  private log(message: string) {
    console.log(`GradeService: ${message}`);
  }

  /**
* Handle Http operation that failed.
* Let the app continue.
* @param operation - name of the operation that failed
* @param result - optional value to return as the observable result
*/
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
