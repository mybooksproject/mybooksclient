import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Grade } from '../models/grade.model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const grades = [
      {
        id: "1", name: "first",
        books: [{ title: "Kannada", category: "Language", file: "location" }, { title: "Maths", category: "Science", file: "location" }]
      },
      {
        id: "2", name: "second",
        books: [{ title: "English", category: "Language", file: "location" }, { title: "Maths", category: "Science", file: "location" }]
      },
      {
        id: "3", name: "third",
        books: [{ title: "Science", category: "Science", file: "location" }]
      },
      {
        id: "4", name: "fourth",
        books: []
      }
    ];
    return { grades };
  }

  genId(grades: Grade[]): string {
    return grades.length > 0 ? String(Math.random() * 100) : String(1);
  }
}
